// Copyright 2014-2016 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// tactical find tactical mistakes.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"bitbucket.org/zurichess/notation"
	"bitbucket.org/zurichess/notation/epd"
	"bitbucket.org/zurichess/notation/fen"
	"bitbucket.org/zurichess/zurichess/engine"
)

var (
	epdPath   = flag.String("epd", "", "file with EPD lines")
	fenPath   = flag.String("fen", "", "file with FEN lines")
	fromDepth = flag.Int("from", 0, "minimum depth to search")
	toDepth   = flag.Int("to", 2, "maximum depth to search")
	diff      = flag.Int("diff", 75, "score difference to consider tactical mistake")
)

func abs(x int32) int32 {
	if x < 0 {
		return -x
	}
	return x
}

func main() {
	log.SetFlags(log.Lshortfile)
	flag.Parse()

	// Create parser.
	var input string
	var parser notation.Parser
	if *epdPath != "" {
		input, parser = *epdPath, &epd.Parser{}
	} else if *fenPath != "" {
		input, parser = *fenPath, &fen.Parser{}
	} else {
		log.Fatal("-epd or -fen must be specified")
	}

	// Open input file.
	var err error
	var fin *os.File
	if fin, err = os.Open(input); err != nil {
		log.Fatalf("cannot open %s for reading: %v", input, err)
	}
	defer fin.Close()

	// Scan file line by line.
	scanner := notation.NewEPDScanner(fin, parser)
	for i := 0; scanner.Scan(); i++ {
		// Reads position from file.
		if err := scanner.Err(); err != nil {
			log.Printf("#%d error: %v", i, err)
			continue
		}
		epd := scanner.EPD()

		pos := epd.Position
		if pos.InsufficientMaterial() {
			// Ignore theoretical draws.
			continue
		}
		if pos.IsChecked(pos.Us()) {
			// Ignore positions where there is a tactical threat.
			continue
		}

		var fromScore, toScore int32
		var fromPV, toPV []engine.Move

		// Play until depth from.
		{
			ai := engine.NewEngine(pos, nil, engine.Options{})
			tc := engine.NewFixedDepthTimeControl(pos, int32(*fromDepth))
			tc.Start(false)
			fromScore, fromPV = ai.Play(tc)

			if len(fromPV) != 0 && pos.GivesCheck(fromPV[0]) {
				// Ignore positions where there is a tactical threat.
				continue
			}
		}

		// Play until depth to.
		quiet := true
		{
			ai := engine.NewEngine(pos, nil, engine.Options{})
			tc := engine.NewFixedDepthTimeControl(pos, int32(*toDepth))
			tc.Start(false)
			toScore, toPV = ai.Play(tc)

			for _, m := range toPV {
				pos.DoMove(m)
				if m.IsViolent() || pos.IsChecked(pos.Us()) {
					quiet = false
				}
			}
			for range toPV {
				pos.UndoMove()
			}
		}

		if quiet && fromScore*toScore <= 0 && abs(fromScore-toScore) >= int32(*diff) {
			fmt.Printf("#%d: from %d (%v) at depth %d, to %d (%v) at depth %d:\n\t\t%s\n",
				i, fromScore, fromPV, *fromDepth, toScore, toPV, *toDepth, pos)
		}
	}
}
