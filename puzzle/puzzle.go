// Copyright 2014-2015 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// puzzle tries to solve puzzles from files.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"

	"bitbucket.org/zurichess/notation"
	"bitbucket.org/zurichess/notation/epd"
	"bitbucket.org/zurichess/notation/fen"
	"bitbucket.org/zurichess/zurichess/board"
	"bitbucket.org/zurichess/zurichess/engine"
)

var (
	epdPath    = flag.String("epd", "", "file with EPD lines")
	fenPath    = flag.String("fen", "", "file with FEN lines")
	output     = flag.String("output", "", "file to write EPD with solutions")
	deadline   = flag.Duration("deadline", 0, "how much time to spend for each move")
	depth      = flag.Int("depth", -1, "search up to max_depth plies")
	verbose    = flag.Bool("verbose", false, "print individual tests")
	cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
)

func main() {
	log.SetFlags(log.Lshortfile)

	// Validate falgs.
	flag.Parse()
	if *cpuprofile != "" { // Enable cpuprofile.
		fin, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(fin)
		defer pprof.StopCPUProfile()
	}

	var input string
	var parser notation.Parser
	if *epdPath != "" {
		input, parser = *epdPath, &epd.Parser{}
	} else if *fenPath != "" {
		input, parser = *fenPath, &fen.Parser{}
	} else {
		log.Fatal("-epd or -fen must be specified")
	}

	var err error
	var fin *os.File
	if fin, err = os.Open(input); err != nil {
		log.Fatalf("cannot open %s for reading: %v", input, err)
	}
	defer fin.Close()

	var fout *os.File
	if *output != "" {
		if fout, err = os.Create(*output); err != nil {
			log.Fatalf("cannot open %s for writing: %v", *output, err)
		}
		defer fout.Close()
	}

	stats := engine.Stats{}
	scanner := notation.NewEPDScanner(fin, parser)

	// Read file line by line.
	solvedTests, numTests := 0, 0
	for i, o := 0, 0; scanner.Scan(); i++ {
		// Reads position from file.
		if err := scanner.Err(); err != nil {
			log.Printf("#%d error: %v", i, err)
			continue
		}
		epd := scanner.EPD()

		// Builds time control.
		var timeControl *engine.TimeControl
		if *deadline != 0 {
			timeControl = engine.NewDeadlineTimeControl(epd.Position, *deadline)
		} else if *depth != -1 {
			timeControl = engine.NewFixedDepthTimeControl(epd.Position, int32(*depth))
		} else {
			log.Fatal("--deadline or --depth must be specified")
		}

		// Evaluate position.
		timeControl.Start(false)
		ai := engine.NewEngine(nil, nil, engine.Options{})
		ai.SetPosition(epd.Position)
		_, actual := ai.Play(timeControl)

		// Update number of solved games.
		numTests++
		var expected board.Move
		for _, expected = range epd.BestMove {
			if expected == actual[0] {
				solvedTests++
				break
			}
		}

		if *verbose {
			// Print a header from time to time.
			if o%25 == 0 {
				fmt.Println()
				fmt.Println("line     bm actual  cache  nodes  correct epd")
				fmt.Println("----+------+------+------+------+--------+---")
			}

			m := "(none)"
			if len(actual) > 0 {
				m = actual[0].String()
			}

			// Print results.
			fmt.Printf("%4d %6s %6s %5.2f%% %5dK %4d/%4d %s\n",
				i+1, expected.String(), m,
				float32(ai.Stats.CacheHit)/float32(ai.Stats.CacheHit+ai.Stats.CacheMiss)*100,
				ai.Stats.Nodes/1000, solvedTests, numTests, epd.String())
			o++
		}

		if fout != nil {
			if len(actual) != 0 {
				epd.BestMove = []board.Move{actual[0]}
			}
			fmt.Fprintln(fout, epd.String())
		}

		// Update stats.
		stats.CacheHit += ai.Stats.CacheHit
		stats.CacheMiss += ai.Stats.CacheMiss
		stats.Nodes += ai.Stats.Nodes
		o++
	}

	fmt.Printf("%s solved %d out of %d ; nodes %d ; cachehit %d out of %d (%.2f%%) ;\n",
		input, solvedTests, numTests, stats.Nodes,
		stats.CacheHit, stats.CacheHit+stats.CacheMiss,
		stats.CacheHitRatio()*100)
}
