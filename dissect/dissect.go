// Copyright 2014-2015 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// static is a tool to analyze the evaluation function.
// +
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	// "strings"

	"bitbucket.org/zurichess/notation"
	"bitbucket.org/zurichess/notation/epd"
	. "bitbucket.org/zurichess/zurichess/engine"
)

var (
	epdPath = flag.String("epd", "/dev/stdin", "file with EPD lines")
)

func main() {
	log.SetFlags(log.Lshortfile)
	flag.Parse()

	fin, err := os.Open(*epdPath)
	if err != nil {
		log.Fatalf("cannot open %s for reading: %v", *epdPath, err)
	}
	defer fin.Close()

	scanner := notation.NewEPDScanner(fin, &epd.Parser{})

	for scanner.Scan() {
		if err := scanner.Err(); err != nil {
			log.Println("scanner.Scan:", err)
			continue
		}

		epd := scanner.EPD()
		pos := epd.Position

		fmt.Println("-------------------------")
		pos.PrettyPrint()

		eval := EvaluatePosition(pos)
		// phase := Phase(pos)

		vals := make(map[string][]int32)
		weights := make(map[string][]int32)
		order := []string{}

		for i, v := range eval.Accum.Values {
			// TODO: Features are expected to be in order.
                        /*
			name := strings.Split(FeatureNames[i], ".")[0]
			if _, has := vals[name]; !has {
				order = append(order, name)
			}
                        */
                        name := fmt.Sprint(i)
                        order = append(order, name)

			// Phase the score to compute the weight of the feature.
                        // w := (int32(v)*Weights[i].M*(256-phase) + int32(v)*Weights[i].E*phase) / 256
			w := v

			vals[name] = append(vals[name], int32(v))
			weights[name] = append(weights[name], int32(w))
		}

		// Print feature values.
		/*
			for _, n := range order {
				v := vals[n]
				if len(v) <= 8 {
					fmt.Printf("%20s % 6d\n", n, v)
				} else {
					fmt.Printf("%20s % 6d\n", n, v[:8])
					for s := 8; s < len(v); s += 8 {
						e := s + 8
						if e > len(v) {
							e = len(v)
						}
						fmt.Printf("%20s % 6d\n", "", v[s:e])
					}
				}
			}
			fmt.Println()
		*/

		// Print feature weights
		for _, n := range order {
			v := weights[n]
			if len(v) <= 8 {
				fmt.Printf("%20s % 6d\n", n, v)
			} else {
				fmt.Printf("%20s % 6d\n", n, v[:8])
				for s := 8; s < len(v); s += 8 {
					e := s + 8
					if e > len(v) {
						e = len(v)
					}
					fmt.Printf("%20s % 6d\n", "", v[s:e])
				}
			}
		}
		fmt.Println()
	}
}
