package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"sort"

	"bitbucket.org/zurichess/notation"
	"bitbucket.org/zurichess/notation/epd"
	. "bitbucket.org/zurichess/zurichess/board"
)

var (
	epdPath = flag.String("epd", "", "input file")
)

type datum struct {
	pos    *Position
	result float64
}

// max returns maximum of a and b.
func max(a, b int32) int32 {
	if a >= b {
		return a
	}
	return b
}

// mvvlva values based on one pawn = 10.
var mvvlvaBonus = [...]int16{0, 10, 40, 45, 68, 145, 256}

// mvvlva computes Most Valuable Victim / Least Valuable Aggressor
// https://chessprogramming.wikispaces.com/MVV-LVA
func mvvlva(m Move) int16 {
	a := m.Target().Figure()
	v := m.Capture().Figure()
	return mvvlvaBonus[v]*64 - mvvlvaBonus[a]
}

var pawnPSQT = [64]int32{
	0, 0, 0, 0, 0, 0, 0, 0,
	5, 10, 10, -20, -20, 10, 10, 5,
	5, -5, -10, 0, 0, -10, -5, 5,
	0, 0, 0, 20, 20, 0, 0, 0,
	5, 5, 10, 25, 25, 10, 5, 5,
	10, 10, 20, 30, 30, 20, 10, 10,
	50, 50, 50, 50, 50, 50, 50, 50,
	0, 0, 0, 0, 0, 0, 0, 0,
}

var knightPSQT = [64]int32{
	-50, -40, -30, -30, -30, -30, -40, -50,
	-40, -20, 0, 0, 0, 0, -20, -40,
	-30, 0, 10, 15, 15, 10, 0, -30,
	-30, 5, 15, 20, 20, 15, 5, -30,
	-30, 0, 15, 20, 20, 15, 0, -30,
	-30, 5, 10, 15, 15, 10, 5, -30,
	-40, -20, 0, 5, 5, 0, -20, -40,
	-50, -40, -30, -30, -30, -30, -40, -50,
}

// The Evaluation of Material Imbalances (by IM Larry Kaufman)
// https://www.chess.com/article/view/the-evaluation-of-material-imbalances-by-im-larry-kaufman
func evalSide(pos *Position, col Color) int32 {
	s := int32(0)
        s += pos.ByPiece(col, Pawn).Count() * 100
        s += pos.ByPiece(col, Knight).Count() * 350
	s += pos.ByPiece(col, Bishop).Count() * 350
	s += pos.ByPiece(col, Rook).Count() * 550
	s += pos.ByPiece(col, Queen).Count() * 1000

	p := pos.ByPiece(col, Pawn).Count()
	s += pos.ByPiece(col, Knight).Count() * (p - 5) * 100 / 16
	s -= pos.ByPiece(col, Rook).Count() * (p - 5) * 100 / 8

	if b := pos.ByPiece(col, Bishop).Count(); b >= 2 {
		s += 50
	}

        /*
	for bb := pos.ByPiece(col, Pawn); bb != 0; {
		sq := bb.Pop().POV(col)
		s += 100 + pawnPSQT[sq]
	}
	for bb := pos.ByPiece(col, Knight); bb != 0; {
		sq := bb.Pop().POV(col)
		s += 315 + knightPSQT[sq]
	}
        */

	return s
}

const cacheSize = 1 << 24

var cacheZobrist [cacheSize]uint64
var cacheScore [cacheSize]int32
var cacheHit, cacheMiss int64

func eval(pos *Position) int32 {
	z := pos.Zobrist()
	if cacheZobrist[z%cacheSize] == z {
		cacheHit++
		return cacheScore[z%cacheSize]
	}

	s := evalSide(pos, White) - evalSide(pos, Black)
	cacheMiss++
	cacheZobrist[z%cacheSize] = z
	cacheScore[z%cacheSize] = s
	return s
}

func searchQuiescence(pos *Position, depth, α, β int32) (Move, int32) {
	us := pos.Us()
	localα := α

	if depth <= 0 {
		static := eval(pos) * pos.Us().Multiplier()
		localα = max(α, static)
		if !pos.IsChecked(pos.Us()) && static >= β {
			// Stand pat if static score is already a cut-off.
			return NullMove, static
		}
	}

	var bestPV Move
	var moves []Move

	if depth > 0 {
		pos.GenerateMoves(Quiet|Violent, &moves)
	} else {
		pos.GenerateMoves(Violent, &moves)
	}

	sort.Slice(moves, func(i, j int) bool {
		return mvvlva(moves[i]) > mvvlva(moves[j])
	})

	for _, move := range moves {
		// Discard illegal or losing captures.
		pos.DoMove(move)
		if pos.IsChecked(us) {
			pos.UndoMove()
			continue
		}
		_, score := searchQuiescence(pos, depth-1, -β, -localα)
		score = -score
		pos.UndoMove()

		if score >= β {
			return move, score
		}
		if score > localα {
			localα = score
			bestPV = move
		}
	}

	return bestPV, localα
}

func main() {
	flag.Parse()
	log.SetFlags(log.Lshortfile | log.Ltime)

	// Read input
	if *epdPath == "" {
		log.Fatal("Missing --epd flag")
	}
	f, err := os.Open(*epdPath)
	if err != nil {
		log.Fatalln("os.Open:", err)
	}
	defer f.Close()

	var save []*notation.EPD
	num, step := 0, 100000

	scanner := notation.NewEPDScanner(f, &epd.Parser{})
epdLoop:
	for ; scanner.Scan(); num++ {
		if err := scanner.Err(); err != nil {
			log.Println("scanner.Scan:", err)
			continue
		}

		epd := scanner.EPD()
		pos := epd.Position
		numMoves := 4

		s1 := eval(pos)
		for i := 0; i < numMoves; i++ {
			move1, _ := searchQuiescence(pos, 1, -100000, +100000)
			if move1 == NullMove {
				continue epdLoop
			}
			pos.DoMove(move1)
		}

		s2 := eval(pos)
		for i := 0; i < numMoves; i++ {
			pos.UndoMove()
		}

                // Check the score.
		if !(s1-50 < s2 && s2 < s1+50) {
			continue epdLoop
		}

		save = append(save, epd)
		if num > step {
                        step += 100000
			log.Println("Read", num, "positions found", len(save), "quiet")
		}
	}

	for _, v := range save {
		fmt.Println(v)
	}

	log.Println("Read", num, "positions found", len(save), "quiet")
	log.Println("Cache", cacheHit, cacheHit+cacheMiss, cacheHit*1000/(cacheHit+cacheMiss))
}
