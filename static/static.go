// Copyright 2014-2015 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// static is a tool to analyze changes of the static evaluation function.
package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"os"

	"bitbucket.org/zurichess/notation"
	"bitbucket.org/zurichess/notation/epd"
	. "bitbucket.org/zurichess/zurichess/engine"
)

var (
	epdPath    = flag.String("epd", "", "file with EPD lines")
	printDiff  = flag.Bool("print_diff", false, "true to print evaluation difference")
	printError = flag.Bool("print_error", false, "true to print evaluation error")
	minError   = flag.Float64("min_error", 1e-2, "minimum error for a position to be printed")
	coef       = flag.Float64("coef", 0.006, "coefficient for static score scaling")

	c9ToResult = map[string]float64{
		"":        0.5,
		"1":       1,
		"1-0":     1,
		"0.5":     0.5,
		"1/2-1/2": 0.5,
		"0":       0,
		"0-1":     0,
	}
)

func sigmoid(x float64) float64 {
	return 1 / (1 + math.Exp(-x))
}

func sqrt(x float64) float64 {
	return x * x
}

func main() {
	log.SetFlags(log.Lshortfile)
	flag.Parse()

	fin, err := os.Open(*epdPath)
	if err != nil {
		log.Fatalf("cannot open %s for reading: %v", *epdPath, err)
	}
	defer fin.Close()

	scanner := notation.NewEPDScanner(fin, &epd.Parser{})
	loss := float64(0.)
	better, worse := 0, 0
	num := 0

	for ; scanner.Scan(); num++ {
		if err := scanner.Err(); err != nil {
			log.Println("scanner.Scan:", err)
			continue
		}
		epd := scanner.EPD()
		result := c9ToResult[epd.Comment["c9"]]

		oldStatic := int32(0)
		if epd.Evaluation != nil {
			oldStatic = *epd.Evaluation
		}
		oldDelta := sqrt(result - sigmoid(float64(oldStatic)**coef))

		static := Evaluate(epd.Position).GetCentipawnsScore()
		epd.Evaluation = &static
		delta := sqrt(result - sigmoid(float64(static)**coef))
		loss += delta

		ε := 1e-2
		if delta < oldDelta-ε {
			better++
		} else if delta > oldDelta+ε {
			worse++
		}

		if delta < *minError {
			continue
		}
		if *printDiff {
			fmt.Printf("%+7.5f %+6d ", delta-oldDelta, static-oldStatic)
		}
		if *printError {
			fmt.Printf("%+7.5f %+6d ", delta, static)
		}
		fmt.Println(epd)
	}

	log.Println("average loss", loss/float64(num), "better", better, "worse", worse)
}
